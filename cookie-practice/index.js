/**
노드는 수정시 서버 재실행해야 
쿠키등록 삭제 예
쿠키읽기 예
*/

// 익스프레스에서 쿠키를 사용하려면 cookie-parser 라는 패키지를 사용해야한다.
const cookieParser = require('cookie-parser')

const express = require('express')

const app = express()

app.use(cookieParser())
app.get('/', (req, res, next) => {
  console.log(req.cookies, req.cookies.name)
  res.cookie('name', 'express')
  res.cookie('name2', 'express2')
  res.cookie('name3', 'express3')
  res.send('HOME')
})

app.get('/expire', (req, res, next) => {
  res.cookie('name4', 'express4', { maxAge: 10000, path: '/expire' }) //10초간 유지 옵션값은 이밖에도 많이 있음
  res.send('DELETE COOKIE')
})

app.get('/delCookie', (req, res, next) => {
  res.clearCookie('name')
  res.send('DELETE COOKIE')
})

app.listen(3060, () => {
  console.log('server on 3060')
})
