const express = require('express')
const path = require('path')
const session = require('express-session')
const FileStore = require('session-file-store')(session)
const { isLogged } = require('./middleware')
require('dotenv').config()

const FAKE_USER = {
  id: 'abc',
  password: '1234',
  nickname: '홍길동'
}

const app = express()
app.set('view engine', 'pug')
app.set('views', path.join(__dirname, './views'))
//parse json body : bodyparser
app.use(express.json())
// url encoded body
app.use(express.urlencoded({ extended: false }))
app.use(
  session({
    resave: false,
    saveUninitialized: true,
    secret: process.env.SECRET,
    store: new FileStore()
  })
)

app.get('/', (req, res, next) => {
  console.log(req.session)
  res.status(200).render('./index', {
    isLogged: req.session && req.session.isLogged,
    nickname: req.session && req.session.nickname
  })
})
app.post('/post', isLogged, (req, res, next) => {
  console.log(req.body)
  const { title, content } = req.body
  res
    .status(200)
    .render('./content', { title, content, nickname: req.session.nickname })
})
app.get('/post', isLogged, (req, res, next) => {
  const { nickname, isLogged } = req.session
  res.status(200).render('post', { nickname, isLogged })
})
app.post('/login', (req, res, next) => {
  const { id, password } = req.body
  //사용자정보 (이메일, 비번 혹은 그 외 방법)가 일치하게 되면
  if (id === FAKE_USER.id && password === FAKE_USER.password) {
    //세션에 사용자 필요한 정보를 등록한다
    req.session.isLogged = true
    req.session.nickname = FAKE_USER.nickname
    console.log('세션', req.session)
    res.status(200).render('./post', {
      nickname: req.session.nickname,
      isLogged: req.session.isLogged
    })
  } else {
    res.status(401).render('./error')
  }
})
app.get('/logout', async (req, res, next) => {
  req.session.destroy((err) => {
    if (err) {
      console.error(err)
    }
    res.status(200).redirect('/')
  })
})

app.get('*', (req, res, next) => {
  res.status(404).send('Not found page')
})
app.use((err, req, res, next) => {
  console.error(err)
  res.status(500).send('somethings broken')
})
app.listen(3065, () => {
  console.log('server on 3065')
})
