module.exports = {
  isLogged: (req, res, next) => {
    if (req.session && req.session.isLogged) {
      next()
    } else {
      res.status(401).redirect('/')
    }
  }
}
