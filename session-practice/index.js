const express = require('express')
const session = require('express-session')
const FileStore = require('session-file-store')(session)
const app = express()
require('dotenv').config()
/**
secret: session ID cookie를 만들때 필요한 필수 옵션값. 반드시 dotenv 사용해야함.
resave: false 세션데이터가 변경되지 않으면 저장소에 값을 저장하지 않는다. true이면 세션데이터가 변경되든 안되든 저장소에 값을 새로 저장한다.
saveUninitialized: 
  false: empty session이 쌓이는걸 방지해 서버 스토리지를 아낄 수 있다.
  true: uninitialized상태의 session을 강제로 저장한다. 즉 아무내용 없는 세션이 계속 저장될 수 있다. 클라이언트의 서버 방문횟수에 따라 등급을 달리하고싶을 때 사용할 수 있음. 왜냐면 세션이 계속 저장되니깐
*/

/**
express-sesion 미들웨어를 설치시에 request 객체에 속성에 session이라고 하는 객체를 추가해준다.
설치안하면 req.session 은 undefined
*/

app.use(
  session({
    secret: process.env.SECRET,
    saveUninitialized: true,
    resave: false,
    store: new FileStore()
  })
)

app.get('/', (req, res, next) => {
  console.log(req.session)
  if (req.session.num === undefined) {
    req.session.num = 1
  } else {
    req.session.num += 1
  }
  res.send(`VIEWS: ${req.session.num}`)
})

app.listen(3065, () => {
  console.log('server on 3065')
})
